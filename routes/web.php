<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::post('/upload', 'IndexController@upload')->name('upload');
Route::post('/edit', 'IndexController@edit')->name('edit');
Route::get('/delete/{id}', 'IndexController@delete')->name('delete');

