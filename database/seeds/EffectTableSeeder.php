<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EffectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('effects')->insert([
            'name' => "Default",
            'class' => "",
        ]);
        DB::table('effects')->insert([
            'name' => "1997",
            'class' => "_1997",
        ]);

        DB::table('effects')->insert([
            'name' => "Aden",
            'class' => "aden",
        ]);

        DB::table('effects')->insert([
            'name' => "Brannan",
            'class' => "brannan",
        ]);
        DB::table('effects')->insert([
            'name' => "Brooklyn",
            'class' => "brooklyn",
        ]);
        DB::table('effects')->insert([
            'name' => "Clarendon",
            'class' => "clarendon",
        ]);
        DB::table('effects')->insert([
            'name' => "Earlybird",
            'class' => "earlybird",
        ]);
        DB::table('effects')->insert([
            'name' => "Gingham",
            'class' => "gingham",
        ]);
        DB::table('effects')->insert([
            'name' => "Hudson",
            'class' => "hudson",
        ]);
        DB::table('effects')->insert([
            'name' => "Inkwell",
            'class' => "inkwell",
        ]);
        DB::table('effects')->insert([
            'name' => "Kelvin",
            'class' => "kelvin",
        ]);
        DB::table('effects')->insert([
            'name' => "Lark",
            'class' => "lark",
        ]);
        DB::table('effects')->insert([
            'name' => "Mayfair",
            'class' => "mayfair",
        ]);
        DB::table('effects')->insert([
            'name' => "Moon",
            'class' => "moon",
        ]);
        DB::table('effects')->insert([
            'name' => "Nashville",
            'class' => "nashville",
        ]);
        DB::table('effects')->insert([
            'name' => "Perpetua",
            'class' => "perpetua",
        ]);
        DB::table('effects')->insert([
            'name' => "Reyes",
            'class' => "reyes",
        ]);
        DB::table('effects')->insert([
            'name' => "Rise",
            'class' => "rise",
        ]);
        DB::table('effects')->insert([
            'name' => "Slumber",
            'class' => "slumber",
        ]);
        DB::table('effects')->insert([
            'name' => "Stinson",
            'class' => "stinson",
        ]);
        DB::table('effects')->insert([
            'name' => "Toaster",
            'class' => "toaster",
        ]);
        DB::table('effects')->insert([
            'name' => "Valencia",
            'class' => "valencia",
        ]);
        DB::table('effects')->insert([
            'name' => "Walden",
            'class' => "walden",
        ]);
        DB::table('effects')->insert([
            'name' => "Willow",
            'class' => "willow",
        ]);
        DB::table('effects')->insert([
            'name' => "Xpro2",
            'class' => "xpro2",
        ]);
    }
}
