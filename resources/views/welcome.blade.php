<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <link rel="stylesheet" href="css/uikit.min.css"/>
    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/cssgram.min.css')}}"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.css" rel="stylesheet">
    <script src="js/jquery.min.js"></script>
    <script src="js/uikit.min.js"></script>
    <script src="js/uikit-icons.min.js"></script>
    <script src="js/jquery.imgcheckbox.js"></script>
    <!-- Fonts -->
</head>
<body>
<br>
<div class="uk-container">
    <div uk-grid>
        <!-- Left panel - logo + title
        <div class="uk-width-1-4">
            <img src="http://via.placeholder.com/270x150" alt="logo">
            <div class="uk-width-1-1">
                <ul class="uk-nav uk-nav-default">
                    <li class="uk-nav-header">
                        CFPT Informatique
                    </li>
                    <li><a href="#">Accueil</a></li>
                    <li><a href="#">Publications</a></li>
                    <li><a href="#">Vidéos</a></li>
                    <li><a href="#">Photos</a></li>
                </ul>
            </div>
        </div> !-->

        <!-- Left panel - banner + posts !-->
        <div class="uk-width-1-1">
            <img src="{{asset('img/test.png')}}" alt="bannière">
            @include('layouts.errors')
            @include('layouts.succcess')


            <ul uk-tab="animation: uk-animation-slide-bottom-small">
                <li class="uk-active"><a href="#">Fil d'actualité</a></li>
                <li><a href="#">Create a post</a></li>
            </ul>
            <ul id="my-id" class="uk-switcher">
                <li>
                    @each('layouts.post', $posts, 'post')
                </li>
                <li>
                    <form action="{{route('upload')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <legend class="uk-legend">Content</legend>
                        <div class="uk-width-1-1 uk-margin-small">
                            <input class="uk-input" placeholder="Title" type="text" name="title" required>
                        </div>
                        <textarea name="content" class="uk-textarea" required placeholder="Content..."
                                  rows="5"></textarea>

                        <div class="uk-width-1-1 uk-margin-small" uk-margin>
                            <div uk-form-custom="target: true">
                                <input type="file" name="images[]" multiple accept="image/*">
                                <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file"
                                       disabled>
                            </div>
                            <button class="uk-button uk-button-primary" type="submit">Save</button>

                        </div>
                        <div class="uk-width-1-1">
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </div>
</div>
@each('layouts.modalDelete',$posts,'post')
@foreach($posts as $post)
    @include('layouts.modalEdit')
@endforeach
</body>
<script>
    $(".imgcheckbox").imgCheckbox({
        "preselectAll": true,
        "graySelected": false
    });
    @foreach($posts as $post)
        @foreach($post->pictures as $picture)
        $("#filter{{$picture->id}}").val("{{$picture->effect->class}}");
        $("#figure{{$picture->id}}").addClass("{{$picture->effect->class}}");
        $("#filter{{$picture->id}}").click(function () {
            $("#filter{{$picture->id}}").change(function () {
                $("#figure{{$picture->id}}").removeClass();
                $("#figure{{$picture->id}}").addClass( $("#filter{{$picture->id}}").val().toLowerCase());
            });
        });
        @endforeach
    @endforeach

</script>
</html>
