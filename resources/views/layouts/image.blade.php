<div>
    <div>
        <a class="uk-inline" href="{{url('storage/'.$picture->path)}}">
            <figure class="{{$picture->effect->class}}">
            <img src="{{url('storage/'.$picture->thumbnailPath)}}" alt="">
            </figure>
        </a>
        @if(!$picture->metadata  == "")
            <button class="uk-button uk-button-medium uk-button-default uk-margin-small-top uk-width-1-1"
                    type="button" uk-toggle="target: #modal{{$picture->id}}">Metadata
            </button>
        @else
            <button class="uk-button uk-button-medium uk-margin-small-top uk-width-1-1"
                    type="button" disabled >No metadata  :(
            </button>
        @endif
    </div>
</div>

<div id="modal{{$picture->id}}" uk-modal>
    <div class="uk-modal-dialog uk-width-auto">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Metadata</h2>
        </div>
        <div class="uk-modal-body">
            <table class="uk-table uk-table-striped uk-table-small uk-table-responsive">
                @foreach($picture->metadata as $key1 => $value)
                    @if(!is_array($value))
                        <tr>
                            <td><b>{{$key1}}</b></td>
                            <td>{{$value}}</td>
                        </tr>
                    @else
                        <tr>
                            <td colspan="2"><b>{{$key1}}</b></td>
                        </tr>
                        @foreach($value as $key2 => $value2)
                            <tr>
                                <td><b><i>{{$key2}}</i></b></td>
                                <td>{{$value2}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                    @endif
                @endforeach
            </table>
        </div>
        <div class="uk-modal-footer">
            <button class="uk-button uk-button-default uk-modal-close uk-button-danger uk-align-right" type="button">Close</button>
        </div>
    </div>
</div>