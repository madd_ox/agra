<div id="modalEdit{{$post->id}}" class="uk-modal-full uk-flex-top"
     uk-modal="{bgclose:false, modal: false, keyboard: false}">
    <div class="uk-modal-dialog uk-margin-auto-vertical uk-width-1-2">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Editing post {{$post->id}}</h2>
        </div>
        <form action="{{route('edit')}}" method="post" enctype="multipart/form-data">
            <div class="uk-modal-body">
                <input type="hidden" value="{{$post->id}}" name="id">
                {{csrf_field()}}
                <legend class="uk-legend">Content</legend>
                <div class="uk-width-1-1 uk-margin-small">
                    <input value="{{$post->title}}" class="uk-input" placeholder="Title" type="text" name="title"
                           required>
                </div>
                <textarea name="content" class="uk-textarea" required rows="5">{{$post->content}}</textarea>
                <div class="uk-margin" uk-form-custom="target: true">
                    <input type="file" name="newimages[]" multiple accept="image/*">
                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Select file"
                           disabled>
                </div>
                <div class="uk-h3">Pictures</div>
                <div uk-grid class="uk-child-width-auto uk-grid-small">
                    @foreach($post->pictures as $picture)
                        @include('layouts.imageEdit')
                    @endforeach
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <a class="uk-button uk-button-default uk-modal-close uk-close-large" type="button">Cancel</a>
                <button type="submit" class="uk-button uk-button-primary">Save</button>
            </div>
        </form>
    </div>
</div>