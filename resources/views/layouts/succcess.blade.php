@if (Session('success'))
    <div class="uk-alert-success" uk-alert>
        <a class="uk-alert-close" uk-close></a>
        <p><strong>{!! Session('success')!!}</strong></p>
    </div>
@endif