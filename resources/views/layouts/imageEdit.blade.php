<div class="uk-inline">
    <figure class="" id="figure{{$picture->id}}">
    <img name="images[{{$picture->id}}]" class="imgcheckbox" src="{{url('storage/'.$picture->thumbnailPath)}}" alt="">
    </figure>
    <select class="uk-select" name="effects[{{$picture->id}}]" id="filter{{$picture->id}}">
        @each('layouts.effect', $effects, 'effect')
    </select>
</div>