<div id="modalDelete{{$post->id}}" uk-modal class="uk-alert- uk-flex-top">
    <div class="uk-modal-dialog uk-margin-auto-vertical">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-header">
            <h2 class="uk-modal-title">Deleting post {{$post->id}}</h2>
        </div>
        <div class="uk-modal-body">
            Are you sur you want to delete this post ?
        </div>
        <div class="uk-modal-footer uk-text-right">
            <a class="uk-button uk-button-default uk-modal-close" type="button">Cancel</a>
            <a href="{{route('delete',['id' => $post->id])}}" class="uk-button uk-button-danger"
               type="button">Delete</a>
        </div>
    </div>
</div>