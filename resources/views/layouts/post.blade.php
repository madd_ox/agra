<article class="uk-article uk-width-auto uk-margin-medium-top uk-margin-medium-bottom">
    <h1 class="uk-article-title"><a class="uk-link-reset" href="">{{$post->title}}</a></h1>
    <div class="uk-float-right uk-display-inline">
        <a class="icon-custom edit" uk-toggle  href="#modalEdit{{$post->id}}"></a>
        <a class="icon-custom trash" uk-toggle  href="#modalDelete{{$post->id}}"></a>
    </div>
    <p class="uk-article-meta">Written by <a href="#">Anonymous</a> on {{$post->created_at}}</p>
    <p>{{$post->content}}</p>
    <div class="uk-h3">Pictures</div>
    <div class="uk-child-width-1-3" uk-grid uk-lightbox="animation: slide">
        @each('layouts.image', $post->pictures, 'picture')
    </div>
</article>
<hr>