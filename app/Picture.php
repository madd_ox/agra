<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;

class Picture extends Model
{
    protected $appends = [
        "metadata"
    ];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }
    public function effect()
    {
        return $this->belongsTo(Effect::class);
    }

    public function getMetadataAttribute()
    {
        if(Image::make(public_path("storage/".$this->path))->exif()!= null)
        {
            return Image::make(public_path("storage/".$this->path))->exif();
        }
        else
        {
            return array();
        }
    }
}
