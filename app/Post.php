<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }
}
