<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageEditRequest;
use App\Http\Requests\ImageUploadRequest;
use Intervention\Image\Facades\Image;
use App\Picture;
use App\Post;
use App\Effect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class IndexController extends Controller
{
    /***
     * Return all the index view with all the posts
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = Post::all();
        $effects = Effect::all();
        return view('welcome', compact('posts','effects'));
    }

    /***
     * Copy an image, for test if there is no right problems....
     * @param $picture
     * @return bool
     */
    public function tryRights($picture)
    {
        try {
            Storage::disk('public')->copy($picture->path, $picture->path . '.tmp');
            Storage::disk('public')->copy($picture->thumbnailPath, $picture->thumbnailPath . '.tmp');
        } catch (\ErrorException $e) {
            return false;
        };
        return true;
    }

    /***
     * Copy and delete an image, for test if there is no right problems....
     * @param $image
     * @return bool
     */
    public function tryUpload($image)
    {
        try {
            $name = Storage::disk('public')->putFile('image', $image);
            Storage::disk('public')->delete($name, $name);
        } catch (\ErrorException $e) {
            return false;
        }
        return true;
    }

    /**
     * Insert an image to the storage
     * @param $image
     * @param $post
     * @return bool
     */
    public function insertImage($image, $post)
    {
        $name = Storage::disk('public')->putFile('image', $image);
        //Destination
        $destinationPath = storage_path('app/public/');

        //Resizing the image (keeping the aspect ratio)
        $thumb_img = Image::make($image->getRealPath())->resize(370, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        //Saving the image
        $position = strrpos($name, '.');
        $thumbnail = substr_replace($name, '_thumb', $position, 0);
        $thumb_img->save($destinationPath . $thumbnail, 100);

        //Creating a new picture
        $picture = new Picture();
        $picture->post_id = $post->id;
        $picture->path = $name;
        $picture->thumbnailPath = $thumbnail;
        $picture->effect_id = 1;
        $picture->save();
    }

    /***
     * Delete a given image on the DB and disk
     * @param $picture
     */
    public function deleteImage($picture)
    {
        Storage::disk('public')->delete($picture->path . '.tmp', $picture->thumbnailPath . '.tmp');
        Storage::disk('public')->delete($picture->path, $picture->thumbnailPath);
        $picture->delete();
    }

    /***
     * Handle the upload function
     * @param ImageUploadRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function upload(ImageUploadRequest $request)
    {
        $uploadedFiles = array();
        //Test method for upload
        foreach ($request->images as $image) {
            array_push($uploadedFiles, $this->tryUpload($image));
        }

        //Check if all the image were successfully updated
        if (in_array(false, $uploadedFiles)) {
            return back()->withErrors(['Something went wrong during upload']);
        } else {
            try {
                DB::beginTransaction();
                //Creating a new post
                $post = new Post();
                $post->title = $request->title;
                $post->content = $request->content;
                $post->save();

                foreach ($request->images as $image) {
                    $this->insertImage($image, $post);
                }
                DB::commit();
            } catch (QueryException $e) {
                DB::rollback();
                return back()->withErrors(['Something went wrong during upload']);
            }

        }

        return back()->with('success', 'Your post was successfully created !');
    }

    /***
     * Delete a post
     * @param $id
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        $array = array();
        $post = Post::findOrFail($id);
        $pictures = $post->pictures;
        foreach ($pictures as $picture) {
            array_push($array, $this->tryRights($picture));
        }

        if (in_array(false, $array)) {
            return back()->withErrors(['Something went wrong during suppression!']);
        } else {
            foreach ($pictures as $picture) {
                $this->deleteImage($picture);
            }
        }

        $post->delete();
        return back()->with('success', 'Post sucessfully deleted !');
    }

    /***
     * Edit a post
     * @param ImageUploadRequest $request
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function edit(ImageEditRequest $request)
    {
        $array = array();

        //Update DB rows
        $post = Post::findOrFail($request->id);
        $pictures = $post->pictures;
        $post->title = $request->title;
        $post->content = $request->content;

        //Upload new pictures
        if ($request->newimages != null) {
            foreach ($request->newimages as $picture) {
                $this->insertImage($picture, $post);
            }
        }

        //Check rights
        foreach ($pictures as $picture) {
            if (!array_key_exists($picture->id, $request->images)) {
                array_push($array, $this->tryRights($picture));
            }

        }

        //Delete unselected pictures
        if (in_array(false, $array)) {
            return back()->withErrors(['Something went wrong during suppression!']);
        } else {
            foreach ($pictures as $picture) {
                if (!array_key_exists($picture->id, $request->images)) {
                    $this->deleteImage($picture);
                }
            }
        }

        //Update effects
        foreach($request->effects as $key=>$effect)
        {
            $effect = Effect::where('class',$effect)->get()->first();
            $picture = Picture::findOrFail($key);
            $picture->effect_id = $effect->id;
            $picture->save();
        }

        //Save if everything is OK
        $post->saveOrFail();

        return back()->with('success', 'Post sucessfully edited !');
    }
}
